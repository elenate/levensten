package com.elenate;

import com.google.common.collect.Multimap;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

public class NameProcessorTest {

    private NameProcessor nameProcessor;
    @Before
    public void setUp() throws Exception {
        nameProcessor = new NameProcessor();
    }

    @Test
    public void testProcessMultipleNames() throws Exception {
        // Given
        List<String> names = new ArrayList<>();
        names.add("Bill Gates");
        names.add("William Henry Gates");
        names.add("Henry Williams");

        // When
        Multimap<String, String> result = nameProcessor.process(names);

        // Then
        assertEquals(3, result.asMap().size());

        Collection<String> duplicates = result.get("Bill Gates");
        assertEquals(1, duplicates.size());

        duplicates = result.get("William Henry Gates");
        assertEquals(2, duplicates.size());

        duplicates = result.get("Henry Williams");
        assertEquals(1, duplicates.size());
    }

    @Test
    public void testProcessNoDuplicates() throws Exception {
        // Given
        List<String> names = new ArrayList<>();
        names.add("Bill Gates");
        names.add("Henry Williams");

        // When
        Multimap<String, String> result = nameProcessor.process(names);

        // Then
        assertEquals(0, result.asMap().size());
    }
}