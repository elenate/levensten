package com.elenate;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Collection;

import static org.junit.Assert.*;


public class FirstNamesRepositoryTest {

    private FirstNamesRepository firstNamesRepository;

    @Before
    public void setUp() throws Exception {
        firstNamesRepository = new FirstNamesRepository();
    }

    @Test
    public void testIsKnownFirstNameForExistingNameVariation() {
        // Given
        String existingNameVariation = "Nick";

        // When
        boolean isKnownFirstName = firstNamesRepository.isKnownFirstName(existingNameVariation);

        // Then
        assertTrue(isKnownFirstName);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIsKnownFirstNameForNull() {
        // Given
        String existingNameVariation = null;

        // When
        firstNamesRepository.isKnownFirstName(existingNameVariation);

        // Then
        // Exception is expected
    }

    @Test
    public void testIsKnownFirstNameForExistingRootName() {
        // Given
        String existingRootName = "Nichole";

        // When
        boolean isKnownFirstName = firstNamesRepository.isKnownFirstName(existingRootName);

        // Then
        assertTrue(isKnownFirstName);
    }

    @Test
    public void testIsKnownFirstNameForNonExistingName() {
        // Given
        String nonExistingName = "Gilliam";

        // When
        boolean isKnownFirstName = firstNamesRepository.isKnownFirstName(nonExistingName);

        // Then
        assertFalse(isKnownFirstName);
    }

    @Test
    public void testGetRootNamesForNameVariationForNonExistingName() {
        // Given
        String nonExistingName = "Gilliam";

        // When
        Collection<String> rootNamesForNameVariation =
                firstNamesRepository.getRootNamesForNameVariation(nonExistingName);

        // Then
        assertEquals(0, rootNamesForNameVariation.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetRootNamesForNameVariationForNull() {
        // Given
        String nonExistingName = null;

        // When
        firstNamesRepository.getRootNamesForNameVariation(nonExistingName);

        // Then
        // Exception is expected
    }

    @Test
    public void testGetRootNamesForNameVariationForRootName() {
        // Given
        String rootName = "Nicholas";

        // When
        Collection<String> rootNamesForNameVariation =
                firstNamesRepository.getRootNamesForNameVariation(rootName);

        // Then
        assertTrue(rootNamesForNameVariation.contains("Nicholas"));
        assertEquals(1, rootNamesForNameVariation.size());
    }

    @Test
    public void testGetNormalizedNamesForNameVariationForVariationWithMultipleRoots() {
        // Given
        String variationWithMultipleRoots = "Nat";

        // When
        Collection<String> rootNamesForNameVariation =
                firstNamesRepository.getRootNamesForNameVariation(variationWithMultipleRoots);

        // Then
        assertTrue(rootNamesForNameVariation.contains("Nathaniel"));
        assertTrue(rootNamesForNameVariation.contains("Nathan"));
        assertTrue(rootNamesForNameVariation.contains("Natasha"));
        assertEquals(3, rootNamesForNameVariation.size());
    }
}