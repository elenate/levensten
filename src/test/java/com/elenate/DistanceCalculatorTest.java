package com.elenate;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class DistanceCalculatorTest {

    private DistanceCalculator distanceCalculator;

    @Before
    public void setUp() throws Exception {
        distanceCalculator = new DistanceCalculator();
    }

    @Test
    public void testIsVariationForVariations() {
        // Given
        String name1 = "Bill";
        String name2 = "William";

        // When
        boolean result = distanceCalculator.isVariation(name1, name2);

        // Then
        assertTrue(result);
    }

    @Test
    public void testIsVariationForRootNames() {
        // Given
        String name1 = "William";
        String name2 = "William";

        // When
        boolean result = distanceCalculator.isVariation(name1, name2);

        // Then
        assertTrue(result);
    }

    @Test
    public void testIsVariationForDifferentNames() {
        // Given
        String name1 = "Bill";
        String name2 = "Nicholas";

        // When
        boolean result = distanceCalculator.isVariation(name1, name2);

        // Then
        assertFalse(result);
    }

    @Test
    public void testModifiedLevenshteinDistanceForVariations() {
        // Given
        String name1 = "Bill";
        String name2 = "William";

        // When
        int result1 = distanceCalculator.getModifiedLevenshteinDistance(name1, name2, 0);
        int result2 = distanceCalculator.getModifiedLevenshteinDistance(name2, name1, 0);

        // Then
        assertEquals(0, result1);
        assertEquals(0, result2);
    }

    @Test
    public void testModifiedLevenshteinDistanceForMisspellingWithinThreshold() {
        // Given
        String name1 = "Gilliams";
        String name2 = "William";

        // When
        int distance = distanceCalculator.getModifiedLevenshteinDistance(name1, name2, 2);

        // Then
        assertEquals(2, distance);
    }

    @Test
    public void testModifiedLevenshteinDistanceForMisspellingWihtoutThreshold() {
        // Given
        String name1 = "Gilliam";
        String name2 = "William";

        // When
        int distance = distanceCalculator.getModifiedLevenshteinDistance(name1, name2, 0);

        // Then
        assertEquals(Integer.MAX_VALUE, distance);
    }


    @Test
    public void testGetMinDistanceSameNames() {
        // Given
        String[] name1 = { "Bill", "Gates" };
        List<String> name1Parts = new ArrayList<>(Arrays.asList(name1));
        String[] name2 = { "Bill", "Gates" };
        List<String> name2Parts = new ArrayList<>(Arrays.asList(name2));

        // When
        int distance = distanceCalculator.getMinDistance(name1Parts, name2Parts, 0);

        // Then
        assertEquals(0, distance);

    }

    @Test
    public void testGetMinDistanceSwitchedParts() {
        // Given
        String[] name1 = { "Bill", "Gates" };
        List<String> name1Parts = new ArrayList<>(Arrays.asList(name1));
        String[] name2 = { "Gates", "Bill" };
        List<String> name2Parts = new ArrayList<>(Arrays.asList(name2));

        // When
        int distance = distanceCalculator.getMinDistance(name1Parts, name2Parts, 0);

        // Then
        assertEquals(0, distance);
    }

    @Test
    public void testGetMinDistanceMissingMiddleName() {
        // Given
        String[] name1 = { "Bill", "Gates" };
        List<String> name1Parts = new ArrayList<>(Arrays.asList(name1));
        String[] name2 = { "Bill", "Henry", "Gates" };
        List<String> name2Parts = new ArrayList<>(Arrays.asList(name2));

        // When
        int distance = distanceCalculator.getMinDistance(name1Parts, name2Parts, 0);

        // Then
        assertEquals(0, distance);
    }

    @Test
    public void testGetMinDistanceShortFirstName() {
        // Given
        String[] name1 = { "William", "Gates" };
        List<String> name1Parts = new ArrayList<>(Arrays.asList(name1));
        String[] name2 = { "Bill", "Gates" };
        List<String> name2Parts = new ArrayList<>(Arrays.asList(name2));

        // When
        int distance = distanceCalculator.getMinDistance(name1Parts, name2Parts, 0);

        // Then
        assertEquals(0, distance);
    }

    @Test
    public void testGetMinDistanceMisspelling() {
        // Given
        String[] name1 = { "Bill", "Gates" };
        List<String> name1Parts = new ArrayList<>(Arrays.asList(name1));
        String[] name2 = { "Bill", "Gats" };
        List<String> name2Parts = new ArrayList<>(Arrays.asList(name2));

        // When
        int distance = distanceCalculator.getMinDistance(name1Parts, name2Parts, 2);

        // Then
        assertEquals(1, distance);
    }

    @Test
    public void testGetMinDistanceCombined() {
        // Given
        String[] name1 = { "Bill", "Gates" };
        List<String> name1Parts = new ArrayList<>(Arrays.asList(name1));
        String[] name2 = { "William", "Henry", "Gats" };
        List<String> name2Parts = new ArrayList<>(Arrays.asList(name2));

        // When
        int distance = distanceCalculator.getMinDistance(name1Parts, name2Parts, 2);

        // Then
        assertEquals(1, distance);
    }

    @Test
    public void testGetMinDistanceAboveThreshold() {
        // Given
        String[] name1 = { "Bill", "Gates" };
        List<String> name1Parts = new ArrayList<>(Arrays.asList(name1));
        String[] name2 = { "Bill", "Gatesss"};
        List<String> name2Parts = new ArrayList<>(Arrays.asList(name2));

        // When
        int distance = distanceCalculator.getMinDistance(name1Parts, name2Parts, 1);

        // Then
        assertEquals(Integer.MAX_VALUE, distance);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetDistanceBetweenNullNames() {
        // Given
        String name1 = null;
        String name2 = "Elena";

        // When
        distanceCalculator.getDistanceBetweenNames(name1, name2, 0);

        // Then
        // Exception is expected
    }

    @Test
    public void testGetDistanceBetweenNamesWithinThreshold() {
        // Given
        String name1 = "William Henry Gates";
        String name2 = "Gilliam Genry Gates";

        // When
        int distance = distanceCalculator.getDistanceBetweenNames(name1, name2, 2);

        // Then
        assertEquals(2, distance);
    }

    @Test
    public void testGetDistanceBetweenNamesWithoutThreshold() {
        // Given
        String name1 = "William Henry Gates";
        String name2 = "Gilliam Genry Gates";

        // When
        int distance = distanceCalculator.getDistanceBetweenNames(name1, name2, 0);

        // Then
        assertEquals(Integer.MAX_VALUE, distance);
    }
}