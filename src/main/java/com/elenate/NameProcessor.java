package com.elenate;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Class with methods that process a list of names and returns their duplicates. Uses
 * {@code DistanceCalculator} to verify if 2 names are duplicates.
 */
public class NameProcessor {

    // the number of errors (misspellings) considered is two
    private static final int MISSPELLING_DISTANCE = 2;

    private DistanceCalculator distanceCalculator;

    public NameProcessor() {
        this.distanceCalculator = new DistanceCalculator();
    }

    /**
     * Returns a multimap (a map containing multiple values per key) of duplicates. The keys are the
     * names, and the values are the duplicates of the names.
     * @param namesList the list of names to be checked
     * @return a multimap containing the names as keys, and their duplicates as values
     */
    public Multimap<String, String> process(List<String> namesList) {

        Set<String> names = new HashSet<>(namesList);
        Multimap<String, String> duplicates = HashMultimap.create(names.size(), 2);

        Iterator<String> namesIterator = names.iterator();
        while (namesIterator.hasNext()) {

            String currentName = namesIterator.next();
            // remove name of list of names to be processed
            namesIterator.remove();
            List<String> duplicatesForName = findDuplicates(currentName, names);
            // add current name's duplicates to the map
            duplicates.putAll(currentName, duplicatesForName);

            // add current name to duplicates of fields not yet processed
            duplicatesForName.parallelStream().forEach(duplicate -> duplicates.put(duplicate, currentName));

        }

        return duplicates;
    }

    /**
     * Finds a list of duplicates in a given list for a given name.
     */
    private List<String> findDuplicates(String currentName, Set<String> names) {
        List<String> duplicatesForName = new ArrayList<>();
        duplicatesForName.addAll(names.parallelStream().filter(name -> areDuplicates(currentName, name))
                                         .collect(Collectors.toList()));

        return duplicatesForName;

    }

    /**
     * If the distance between the names is smaller than what is considered a misspelling distance,
     * the names are duplicates.
     */
    private boolean areDuplicates(String name1, String name2) {
        int distance = distanceCalculator.getDistanceBetweenNames(name1, name2, MISSPELLING_DISTANCE);
        return distance <= MISSPELLING_DISTANCE;

    }

}
