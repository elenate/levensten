package com.elenate;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Class for calculating distances between names. Uses Levenshtein distance calculation and
 * a first name repository to make decisions about first name similarity.
 */
public class DistanceCalculator {

    private static final String SPACE = " ";

    private FirstNamesRepository firstNamesRepository = new FirstNamesRepository();


    /**
     * Gets the distance between two full names, if it is below a given threshold.
     * Two names are considered to have a distance of zero if:
     * - The name parts are the same, but switched: Bill Gates vs Gates Bill
     * - One of the names is missing a middle name: Bill Henry Gates vs Bill Gates
     * - One of the first names is a variation of the other: Bill Gates vs William Gates
     * - Combinations of the above
     * Levenshtein distance is used to calculate distance between strings
     *
     * @param name1
     *         the first name
     * @param name2
     *         the second name
     * @param threshold
     *         the threshold
     *
     * @return the calculated distance between the names, if it is lower than the threshold.
     * Otherwise, {@code Integer.MAX_INT}
     */
    public int getDistanceBetweenNames(String name1, String name2, int threshold) {
        if (name1 == null || name2 == null) {
            throw new IllegalArgumentException("Names must not be null.");
        }
        List<String> name1Parts = new ArrayList<>(Arrays.asList(name1.split(SPACE)));
        List<String> name2Parts = new ArrayList<>(Arrays.asList(name2.split(SPACE)));

        return getMinDistance(name1Parts, name2Parts, threshold);
    }

    /**
     * Compares all the parts of the first parameter with all the parts of the second
     * to find the ones that are most similar. Then it returns the sum of the  distances
     * between the most similar parts, if it is lower than a given threshold.
     * Otherwise returns {@code Integer.MAX_VALUE}.
     */
    int getMinDistance(List<String> name1Parts, List<String> name2Parts, int threshold) {

        int distance = 0;

        // Find the parts of the two names that are most similar by calculating the distance
        // between each pair. Then remove those parts and repeat

        int minI = 0;
        int minJ = 0;
        int minDistance = Integer.MAX_VALUE;

        for (int i = 0; i < name1Parts.size(); i++) {
            for (int j = 0; j < name2Parts.size(); j++) {
                int possibleMin = getModifiedLevenshteinDistance(name1Parts.get(i), name2Parts.get(j), threshold);
                if (possibleMin < minDistance) {
                    minDistance = possibleMin;
                    minI = i;
                    minJ = j;
                }
            }
        }

        // if the min distance or the total distance is longer than the threshold,
        // there is no need to do more processing
        distance = distance + minDistance;
        if (minDistance > threshold || distance > threshold) {
            return Integer.MAX_VALUE;
        }

        name1Parts.remove(minI);
        name2Parts.remove(minJ);
        if (!name1Parts.isEmpty() && !name2Parts.isEmpty()) {
            int remainingMinDistance = getMinDistance(name1Parts, name2Parts, threshold);
            distance = distance + remainingMinDistance;
            if (remainingMinDistance > threshold || distance > threshold) {
                return Integer.MAX_VALUE;
            }
        }

        return distance;
    }

    /**
     * Calculates the Levenshtein distance between two names if it's less than or equal to a given
     * threshold. This method takes into account whether the names are first names and one of them
     * is a variation of the other
     *
     * @param name1
     *         the first name
     * @param name2
     *         the second name
     * @param threshold
     *         the threshold for the distance
     *
     * @return {@code 0}, if the names are first names, and one is a variation of the other. Otherwise,
     * return the distance, if it is lower than or equal to the threshold; otherwise return {@code Integer.MAX_VALUE}
     */
    int getModifiedLevenshteinDistance(String name1, String name2, int threshold) {

        if (firstNamesRepository.isKnownFirstName(name1) && firstNamesRepository.isKnownFirstName(name2)) {
            if (isVariation(name1, name2) || isVariation(name2, name1)) {
                return 0;
            }
        }

        // Levenshtein distance will not be calculated if it is above a certain threshold
        int distance = StringUtils.getLevenshteinDistance(name1, name2, threshold);

        // The StringUtils.getLevenshteinDistance method returns -1 if the distance is above a
        // threshold. Return Integer.MAX_VALUE instead.
        return (distance > -1) ? distance : Integer.MAX_VALUE;

    }

    /**
     * @param nameVariation
     *         the name variation
     * @param rootName
     *         the root name
     *
     * @return true, if the nameVariation is a variation of the rootName
     */
    boolean isVariation(String nameVariation, String rootName) {
        Collection<String> roots = firstNamesRepository.getRootNamesForNameVariation(nameVariation);
        return roots.contains(rootName);

    }


}
