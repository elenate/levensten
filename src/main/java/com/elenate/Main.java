package com.elenate;

import com.google.common.collect.Multimap;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws IOException {

        String fileName = "/Users/eletem/Dev/playground/levenstein/test.txt";
        List<String> names = Files.lines(Paths.get(fileName))
                .collect(Collectors.toList());

        NameProcessor nameProcessor = new NameProcessor();
        Multimap<String, String> result = nameProcessor.process(names);
        System.out.println(result);

    }

}
