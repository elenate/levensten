package com.elenate;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collection;
import java.util.IllegalFormatCodePointException;

/**
 * A repository of first names. The content of the repository is read from a file.
 * Has methods about finding name variations and checking if a name
 * is a first name.
 */
public class FirstNamesRepository {

    private static final String DASH = "-";

    private static final String COMMA = ",";

    private String name = "firstnames.txt";

    // Multimap that contains a first name variation as a key
    // and a list of root names for that variation as a value
    private Multimap<String, String> firstNames = HashMultimap.create();

    /**
     * Constructor.
     */
    public FirstNamesRepository() {
        initializeFirstNamesMap();
    }

    /**
     * Checks if a name is contained in the repository of known first names.
     *
     * @param firstName
     *         the name that is checked, must not be null
     *
     * @return true, if the name is a known first name. Otherwise, false
     *
     * @throws IllegalArgumentException
     *         if the parameter is null
     */
    public boolean isKnownFirstName(String firstName) {
        if (firstName == null) {
            throw new IllegalArgumentException("First name must not be null.");
        }
        return firstNames.containsKey(firstName);
    }

    /**
     * @param nameVariation
     *         the name variation that is checked
     *
     * @return a list of root names for the given name variation
     *
     * @throws IllegalArgumentException
     *         if the parameter is null
     */
    public Collection<String> getRootNamesForNameVariation(String nameVariation) {
        if (nameVariation == null) {
            throw new IllegalArgumentException("Name variation must not be null.");
        }
        return firstNames.get(nameVariation);
    }

    private void initializeFirstNamesMap() {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(name);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        reader.lines().forEach(this::addFirstNameMapEntry);
    }

    /**
     * Adds the entries for the variations of a first name.
     */
    private void addFirstNameMapEntry(String entry) {
        // Line format is:
        // Timothy - Tim, Timmy
        // Tobias - Toby
        String[] names = entry.split(DASH);
        String rootFirstName = names[0].trim();
        // adds the root first name as a variation of itself
        firstNames.put(rootFirstName, rootFirstName);

        String[] firstNameVariations = names[1].split(COMMA);
        // adds the variations as keys, and the root names as values
        Arrays.stream(firstNameVariations)
                .forEach(firstNameVariation -> firstNames.put(firstNameVariation.trim(), rootFirstName));

    }

}
